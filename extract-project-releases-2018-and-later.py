# coding: utf-8

import duckdb

with open('extract-pyproject-releases.sql', 'r') as f:
    QUERY = f.read()

res = duckdb.sql(QUERY)
res.to_csv("extract-project-releases-2018-and-later.csv", header=True)
