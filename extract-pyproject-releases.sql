-- Define the dialect
-- sqlfluff:dialect:duckdb

-- Set a smaller indent for this file
-- sqlfluff:indentation:tab_space_size:2

-- Set keywords to be capitalised
-- sqlfluff:rules:capitalisation.keywords:capitalisation_policy:upper

SELECT
  project_name,
  project_version,
  project_release,
  suffix(project_release, '.whl') AS wheel,
  suffix(project_release, '.tar.gz') AS source,
  max(uploaded_on) AS max_uploaded_on,
  date_part('year', max(uploaded_on)) AS max_year,
  list(DISTINCT uploaded_on)
FROM '*.parquet'
WHERE (date_part('year', uploaded_on) >= '2018') AND skip_reason = ''
GROUP BY project_name, project_version, project_release
